
function Productname(){

    var productname=document.getElementById("productname").value;
    if(productname.length == 0)
        {
            document.getElementById("productname").style=" border:1px solid red ";
            document.getElementById("pnerror").innerHTML="Product Name is required";
            document.getElementById("pnerror").style="visibility:visible;color:red";
            document.getElementById("productname").value;
            return false;
        }
          document.getElementById("productname").style=" ";
          document.getElementById("productname").value;
          document.getElementById("pnerror").style="display:none";
          return true;
}

function Sku(){
    var s=document.getElementById("sku").value;
    if(s.length == 0)
      {
        document.getElementById("sku").style=" border:1px solid red ";
        document.getElementById("skuerror").innerHTML="SKU is required";
        document.getElementById("skuerror").style="visibility:visible;color:red";
        document.getElementById("sku").value;
        return false;
      }
      document.getElementById("sku").style=" ";
      document.getElementById("sku").value;
      document.getElementById("skuerror").style="display:none";
      return true;
}

function Initialstock() {
 var is=document.getElementById("stock").value;
 
    if(is.length == 0)
      {
        document.getElementById("stock").style=" border:1px solid red ";
        document.getElementById("stockerror").innerHTML="Initial Stock is required";
        document.getElementById("stockerror").style="visibility:visible;color:red";
        document.getElementById("stock").value;
        return false;
      }
      document.getElementById("stock").style=" ";
      document.getElementById("stock").value;
      document.getElementById("stockerror").style="display:none";
      return true;
}   
/**
 * 
 */       
function Initialcost() {

 var ic=document.getElementById("cost").value;
 
    if(ic.length == 0)
      {
        document.getElementById("cost").style=" border:1px solid red ";
        document.getElementById("costerror").innerHTML="Initial Cost is required";
        document.getElementById("costerror").style="visibility:visible;color:red";
        document.getElementById("cost").value;
        return false;
      }
      document.getElementById("cost").style=" ";
      document.getElementById("cost").value;
      document.getElementById("costerror").style="display:none";
      return true;

}

function checkproductform() /* It checks whether all required fields are filled on all Product  page */
{
  if(Productname()==true && Sku()== true && Initialstock()== true && Initialcost()== true)
  {
        document.getElementById("sbutton").disabled = false;
  }
  else
  {
    document.getElementById("sbutton").disabled = true;
  }
}

$(document).ready(function() /* Tooltip for Logout button */
{
 $('[data-toggle="tooltip"]').tooltip();   
});

function preview_image(event) /* Preview image on AddProductVariant page */
{
     var reader = new FileReader();
     reader.onload = function()
        {
          var output = document.getElementById('output_image');
          output.src = reader.result;
        }
          reader.readAsDataURL(event.target.files[0]);
}
