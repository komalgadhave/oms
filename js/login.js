function checkloginform()/* It checks whether all required fields are filled on Login page */
{
  if(Businessemail()==true && Pwd()== true)
  {
        document.getElementById("sbutton").disabled = false;
  }
  else
  {
    document.getElementById("sbutton").disabled = true;
  }

}

function checkform() /* It checks whether all required fields are filled on Registration page */
{
	if(Fullname()==true && Businessname()==true && Businessemail()==true && Pwd()== true)
  {
  			document.getElementById("sbutton").disabled = false;
  }
  else
  {
    document.getElementById("sbutton").disabled = true;
   }
}

function checkhelpform() /* It checks whether all required fields are filled on Need Help page */
{
  if(Businessemail()==true)
  {
        document.getElementById("sbutton").disabled = false;
  }
  else
  {
    document.getElementById("sbutton").disabled = true;
  }
}

function Fullname() /* It validates Full Name field on Registration page */
{
  var fname= document.getElementById("fname").value;
   if(fname.length == 0)
  {
    document.getElementById("fname").style=" border:1px solid red ";
    document.getElementById("ferror").innerHTML="Full Name is required";
    document.getElementById("ferror").style="visibility:visible;color:red";
    document.getElementById("fname").value;
    return false;
  }
    document.getElementById("fname").style=" ";
    document.getElementById("fname").value;
    document.getElementById("ferror").style="display:none";
    return true;
}

function Businessname() /*It validates Business Name field on Registration page*/
{
  var bname= document.getElementById("bname").value;
  if(bname.length == 0)
  {
    document.getElementById("bname").style=" border:1px solid red ";
    document.getElementById("berror").innerHTML="Bussiness Name is required";
    document.getElementById("berror").style="visibility:visible;color:red";
    document.getElementById("bname").value;
    return false;
  }
  document.getElementById("bname").style=" ";
  document.getElementById("bname").value;
  document.getElementById("berror").style="display:none";
  return true;
}

function Businessemail() /* It validates Business Email field */
{
  var email= document.getElementById("email").value;
  var atposition=email.indexOf("@");  
  var dotposition=email.lastIndexOf(".");  
    
  if(email.length == 0 ) /* Empty field */
  {
    document.getElementById("email").style=" border:1px solid red ";
    document.getElementById("emailerror").innerHTML="Business Email is required";
    document.getElementById("emailerror").style="visibility:visible;color:red";
    document.getElementById("email").value;
    return false;
  }
  if(atposition<1 || dotposition<atposition+2 || dotposition+2>=email.length)  /* Pattern of email */
  {
    document.getElementById("email").style=" border:1px solid red ";
    document.getElementById("emailerror").innerHTML="Enter valid email ";
    document.getElementById("emailerror").style="visibility:visible;color:red";
    document.getElementById("email").value;
    return false;
  }
  document.getElementById("email").style=" ";
  document.getElementById("email").value;
  document.getElementById("emailerror").style="display:none";
  return true;
}

function Pwd() /* It validates Password field */
{
   var pwd= document.getElementById("pwd").value;
    
  if(pwd.length <8)
  {
    document.getElementById("pwd").style=" border:1px solid red ";
    document.getElementById("pwderror").innerHTML="Minimum 8 characters required";
    document.getElementById("pwderror").style="visibility:visible;color:red";
    document.getElementById("pwd").value;
    return false;
  }
  document.getElementById("pwd").style=" ";
  document.getElementById("pwd").value;
  document.getElementById("pwderror").style="display:none";
  return true;
}

